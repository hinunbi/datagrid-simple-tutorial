#include "infinispan/hotrod/ConfigurationBuilder.h"
#include "infinispan/hotrod/RemoteCacheManager.h"
#include "infinispan/hotrod/RemoteCache.h"
#include "infinispan/hotrod/Version.h"

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <unistd.h>

#include <sasl/saslplug.h>
#include <err.h>

using namespace infinispan::hotrod;

// =================================
 /* Hotrod SASL is based on Cyrus Sasl libraries.
  * Check cyrus docs for more info on how to setup callbacks
  * https://www.cyrusimap.org/sasl/
  */
static int simple(void*  context , int id, const char **result, unsigned *len) {
    *result = *(char**)context;
    if (len)
        *len = strlen(*result);
    return SASL_OK;
}

static int getsecret(void* /* conn */, void*  context, int id, sasl_secret_t **psecret) {
    char *secret_data=*(char**)context;
    size_t len = strlen(secret_data);
    static sasl_secret_t *x;
    x = (sasl_secret_t *) realloc(x, sizeof(sasl_secret_t) + len);
    x->len = len;
    strcpy((char *) x->data, secret_data);
    *psecret = x;
    return SASL_OK;
}

#define TRIM_SPACE " \t\n\v"
inline std::string rtrim(std::string s,const std::string& drop = TRIM_SPACE)
 {
  return s.erase(s.find_last_not_of(drop)+1);
 }
	
char *pusername;
char *psecret;

static std::vector<sasl_callback_t> callbackHandler {
        { SASL_CB_AUTHNAME, (sasl_callback_ft) &simple, &pusername },
        {SASL_CB_PASS, (sasl_callback_ft) &getsecret, &psecret },
        {SASL_CB_LIST_END, NULL, NULL } };

// =================================
const char * get_current_time() ;


int main(int argc, char** argv) {

    	
	printf("start...\n");
	
	char * DATAGRID_SERVICE_HOST  = getenv("DATAGRID_SERVICE_HOST");
	char * DATAGRID_SERVICE_FQDN  = getenv("DATAGRID_SERVICE_FQDN");
	char * DATAGRID_SERVICE_PORT  = getenv("DATAGRID_SERVICE_PORT");
	char * DATAGRID_USER_NAME     = getenv("DATAGRID_USER_NAME");
	char * DATAGRID_USER_PASSWORD = getenv("DATAGRID_USER_PASSWORD");
	char * DATAGRID_CACHE_NAME    = getenv("DATAGRID_CACHE_NAME");

	DATAGRID_SERVICE_HOST  = (char *)(DATAGRID_SERVICE_HOST  == NULL ? "172.30.88.232" : DATAGRID_SERVICE_HOST);
	DATAGRID_SERVICE_FQDN  = (char *)(DATAGRID_SERVICE_FQDN  == NULL ? "infinispan" : DATAGRID_SERVICE_FQDN);
	DATAGRID_SERVICE_PORT  = (char *)(DATAGRID_SERVICE_PORT  == NULL ? "11222" : DATAGRID_SERVICE_PORT);
	DATAGRID_USER_NAME     = (char *)(DATAGRID_USER_NAME     == NULL ? "developer" : DATAGRID_USER_NAME);
	DATAGRID_USER_PASSWORD = (char *)(DATAGRID_USER_PASSWORD == NULL ? "Vy3DcsQA1NqiHht8" : DATAGRID_USER_PASSWORD);
	DATAGRID_CACHE_NAME    = (char *)(DATAGRID_CACHE_NAME    == NULL ? "default" : DATAGRID_CACHE_NAME);

	printf("DATAGRID_SERVICE_HOST  : [%s]\n", DATAGRID_SERVICE_HOST);
	printf("DATAGRID_SERVICE_FQDN  : [%s]\n", DATAGRID_SERVICE_FQDN);
	printf("DATAGRID_SERVICE_PORT  : [%s]\n", DATAGRID_SERVICE_PORT);
	printf("DATAGRID_USER_NAME     : [%s]\n", DATAGRID_USER_NAME);
	printf("DATAGRID_USER_PASSWORD : [%s]\n", DATAGRID_USER_PASSWORD);	 
	printf("DATAGRID_CACHE_NAME    : [%s]\n", DATAGRID_CACHE_NAME);	 

	// Create a configuration for a locally-running server
	ConfigurationBuilder builder;
	pusername = DATAGRID_USER_NAME;
	psecret   = DATAGRID_USER_PASSWORD;
	        
	printf("add Server : [%s:%s]\n", DATAGRID_SERVICE_HOST, DATAGRID_SERVICE_PORT);   
	builder.addServer()
		.host(DATAGRID_SERVICE_HOST)
		.port(atoi(DATAGRID_SERVICE_PORT));
	
	//printf("Hot Rod version : [%s]\n", Configuration::PROTOCOL_VERSION_24);   	
	//builder.protocolVersion(Configuration::PROTOCOL_VERSION_24);
        
	printf("Security saslMechanism : [%s]\n", "DIGEST-MD5");   	
	printf("Security serverFQDN    : [%s]\n", DATAGRID_SERVICE_FQDN);   	
	builder.security()
		.authentication()
		.saslMechanism("DIGEST-MD5")
		.serverFQDN(DATAGRID_SERVICE_FQDN)
		.callbackHandler(callbackHandler)
		.enable();
             
  printf("Initialize the remote cache manager\n");  	
  RemoteCacheManager cacheManager(builder.build(), false);
  
  printf("The remote cache : [%s]\n", DATAGRID_CACHE_NAME);  	
	RemoteCache<std::string, std::string> cache = cacheManager.getCache<std::string, std::string>(DATAGRID_CACHE_NAME, false);
  printf("Connect to the server.\n");  	
  cacheManager.start();
	
	std::string key("key");
  cache.putIfAbsent(key.c_str(), "start");

  long cnt = 0L;
	int  cycle = 10000;  
  while(true) 
  {    	 
    // Store a value
    std::string value;
    value.append("value at ");
    value.append(get_current_time());
    
    // Put(replace) the value
    if((cnt % cycle) == 0) 
    {
    	printf("[%012d] Put cache : [%s], name:value : [%s:%s]\n", cnt, DATAGRID_CACHE_NAME, key.c_str(), value.c_str());
    }
    cache.replace(key.c_str(), value.c_str());
    
    // Get the value and print it out       
    std::auto_ptr<std::string> ret(cache.get(key.c_str()));
   	if((cnt % cycle) == 0) 
    {
    	printf("[%012d] Get cache : [%s], name:value : [%s:%s]\n", cnt, DATAGRID_CACHE_NAME, key.c_str(), ret->c_str());
    }
    cnt++;
  }
  // Stop the cache manager and release all resources
  cacheManager.stop();
  return 0;

}

const char * get_current_time() {

  time_t rawtime;
  struct tm * timeinfo;
  string str;

  time ( &rawtime );
  timeinfo = localtime ( &rawtime );

  str = asctime (timeinfo);
  
  return rtrim(str).c_str();
}
